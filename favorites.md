---
title: Favorites
layout: default
---

# Movies

  * Начало (2010)
  * Доктор Страндж (2016)
  * http://southpark.cc.com/full-episodes/s16e05-butterballs
  * http://southpark.cc.com/full-episodes/s15e08-ass-burgers
  * http://southpark.cc.com/full-episodes/s15e13-a-history-channel-thanksgiving
  * http://southpark.cc.com/full-episodes/s16e07-cartman-finds-love
  * http://southpark.cc.com/full-episodes/s16e10-insecurity
  * http://southpark.cc.com/full-episodes/s18e07-grounded-vindaloop
  * Enter The Void (2009)
  * Looper (2012)
  * Nine miles down (2009)
  * Interstellar (2014)
  * Lucy (2014)
  * Cloud Atlas (2012)
  * Black Mirror (2011)
  * I origins (2014)
  * Absolutely Anything (2015)
  * Don't Look Back (2009)
  * The Usual Suspects (1995)
  * Point Break (2015)
  * Angels Hearts (1987)
  * Half Light (2006)
  * Life Is Beautiful (1997) 
  * La tigre e la neve (2005)
  * John Hagelin - YouTube
  * Dogville (2003)
  * Manderlay (2005)
  * Hero (2002)
  * Izo (2004)
  * Kung Fu Panda 3 (2016)
  * Fearless (1993)
  * Voices (2014)
  * I Married a Witch (1942)
  * Assasins Creed (2016)
  * Jungle (2017)
  * Гоголь Начало (2017)
  * Каматозники (2017)

## Music

  * https://www.youtube.com/playlist?list=PLPy9aa5pBoq-kkV74L_b2Z8JM0OPBBHmW

## Books

  * Трансерфинг Реальности - все (А+)
  * Paulo Khoello - al (А+)l
  * Пелевин - все (А+)
  * Пастернак - Доктор живаго (А+)
  * А. Н. Толстой - Хождение по мукам (А+)
  * А. И. Солженицин - все (А+)
  * Али-Баба и сорок разбойников (А+)
  * Ветхий Завет (А+)
  * Джон Милтон - Потеряный Рай (А+)
  * Кастанеда - все (А+)
  * Кин Кизи - Пролетая над гнездом кукушки (А+)
  * Джей Стивенс - Штурмуя небеса
  * Фил Джексон - Клубная культура
  * А. Сухочев - Гоа-синдром
  * В. А. Чернобров - ВРЕМЯ И ЧЕЛОВЕК
  * Габриэль Гарсия Маркес - Сто лет одиночества (А+)
  * Гесиод - О происхождении богов
  * Стивен Хокинг - Краткая история времени
  * Трактат Витгенштейна
  * Л. Н. Толстой -  Воскресенье (А+)
  * Л. Н. Толстой - Война и мир (А+)
  * М. А. Булгаков - Мастер и Маргарита (А+)
  * М. Ю. Лермонтов - Герой нашего времени (А+)
  * Н. В. Гоголь - Вий (А+)
  * Плутарх - Сравнительные жизнеописания. Александр и Цезарь (А+)
  * 1001 ночь (А+)
  * Ф. М. Достоевский - Братья карамазовы (А+)
  * Ф. М. Достоевский - Униженные и оскорбленные (А+)
  * Ф. М. Достоевский - Записки из мертвого дома (А+)
  * Харуки Мураками - 1Q84 - 1,2 (А+)
  * Ши Най-ань - Речные заводи (А+)
  * А. П. Чехов - Чайка (А+)
  * А. П. Чехов - Вишневый сад (А+)
  * А. П. Чехов - Три сестры (А+)
  * А. П. Чехов - Дядя Ваня (А+)
  * Роберт Шекли - Все  (А+)
  * Лейнстер Мюррей - Время умирать
  * Генри Каттнер, Кэтрин Л. Мур. - Жил-был гном (A+)
  * Харлан Эллисон - Видение
  * Кристофер Мур - все
  * Ганс - Снежная Королева
  * Туве Янссен - Муми-тролль
  * Волшебник изумрудного города
  * Фунтик

